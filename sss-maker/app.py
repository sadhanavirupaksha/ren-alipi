from flask import Flask, request, render_template, flash, make_response, session, abort, jsonify, g, url_for, send_from_directory, \
    redirect
import conf

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def main():
	if request.method == 'GET':
		return render_template('ren.html')

@app.route('/test', methods=['GET'])
def testApi():
	return "Its working...!"

if __name__ == '__main__':
    app.run(host=conf.HOST[0], port=conf.PORT[0], debug = True)