#! bash script for setting up enviornment for flask app

sudo apt-get install python-virtualenv

virtualenv rnrserver-venv

rnrserver-venv/bin/pip install flask

rnrserver-venv/bin/pip install -U flask-cors

rnrserver-venv/bin/pip install requests

rnrserver-venv/bin/pip install lxml
