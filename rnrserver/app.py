from flask import Flask, request, render_template, flash, make_response, session, abort, jsonify, g, url_for, send_from_directory, \
    redirect
from flask_cors import CORS
from urllib.parse import unquote, quote
import urllib.request as urllib2
import lxml.html
import conf
import requests
from io import StringIO

app = Flask(__name__)
CORS(app)

app.config['SECRET_KEY'] = conf.SECRET_KEY[0]

@app.route('/')
def start_page():
    if 'verified' in request.cookies and request.cookies['verified'] == 'True':
        d = {}
        d['foruri'] = request.args['foruri']
        myhandler1 = urllib2.Request(d['foruri'],
                                     headers={'User-Agent':
                                              "Mozilla/5.0 (X11; " +
                                              "Linux x86_64; rv:25.0)" +
                                              "Gecko/20100101 Firefox/25.0)"})
        # a = urllib2.urlopen(myhandler1)
        # page = a.read()
        # return page
        try:
            a = urllib2.urlopen(myhandler1)
            if a.geturl() != d['foruri']:
                return ("There was a server redirect, please click on the" +
                        " <a href='http://127.0.0.1:5000?foruri={0}'>link</a> to" +
                        " continue.".format(quote(a.geturl())))
            else:
                page = a.read()
                a.close()
        except ValueError:
            return ("The link is malformed, click " +
                    "<a href='http://127.0.0.1:5000/?foruri={0}&lang={1}" +
                    "&interactive=1'>" +
                    "here</a> to be redirected.".format(
                        quote(unquote(d['foruri'].encode('utf-8'))),
                        request.args['lang']))
        except urllib2.URLError:
            return render_template('error.html')
        try:
            print("*******Error1***************")
            page = str(page, 'utf-8')
            print("************Not Error1************") 
        except UnicodeDecodeError:
            pass 
        try:
            print("*****Error2*************")
            g.root = lxml.html.parse(StringIO(page)).getroot()
            print("**********Not Error2*************")
        except ValueError:
            g.root = lxml.html.parse(d['foruri']).getroot()
        
        if(('lang' not in request.args) and ('blog' not in request.args)):
        #if request.args.has_key('lang') == False and request.args.has_key('blog') == False:
            print ("************inside first if********************")
            g.root.make_links_absolute(d['foruri'], resolve_base_href=True)
            print (page)
            for i in g.root.iterlinks():
                if i[1] == 'href' and i[0].tag != 'link':
                    try:    
                        #i[0].attrib['href'] = 'http://{0}?foruri={1}'.format(conf.DEPLOYURL[0], quote_plus(i[0].attrib['href']))
                        i[0].attrib['href'] = 'http://{0}?foruri={1}'.format('127.0.0.1:5000/', quote(i[0].attrib['href']))
                    except KeyError:
                        #i[0].attrib['href'] = '{0}?foruri={1}'.format(conf.DEPLOYURL[0], quote_plus(i[0].attrib['href'].encode('utf-8')))
                        i[0].attrib['href'] = '{0}?foruri={1}'.format('127.0.0.1:5000/', quote(i[0].attrib['href'].encode('utf-8')))
            setScripts()
            g.root.body.set("onload", "annoletContainer();")

        elif request.args.has_key('lang') == True and request.args.has_key('interactive') == True and request.args.has_key('blog') == False:
            setScripts()
            setSocialScript()
            g.root.body.set("onload", "a11ypi.ren();a11ypi.tweet(); " +
                            "a11ypi.facebook(); a11ypi.loadOverlay();")
            g.root.make_links_absolute(d['foruri'], resolve_base_href=True)
            response = make_response()
            response.data = lxml.html.tostring(g.root)
            return response

        elif request.args.has_key('lang') == True and request.args.has_key('blog') == False:
            script_jq_mini = g.root.makeelement('script')
            g.root.body.append(script_jq_mini)
            script_jq_mini.set("src", conf.JQUERYURL[0] + "/jquery.min.js")
            script_jq_mini.set("type", "text/javascript")
            d['lang'] = request.args['lang']
            script_test = g.root.makeelement('script')
            g.root.body.append(script_test)
            script_test.set("src", conf.APPURL[0] + "/alipi/ui.js")
            script_test.set("type", "text/javascript")
            g.root.body.set("onload", "a11ypi.ren()")

        elif request.args.has_key('interactive') == True and request.args.has_key('blog') == True and request.args.has_key('lang') == True:
            setScripts()
            setSocialScript()
            g.root.body.set("onload", "a11ypi.filter(); a11ypi.tweet();" +
                            "a11ypi.facebook(); a11ypi.loadOverlay();")
            g.root.make_links_absolute(d['foruri'], resolve_base_href=True)

        elif request.args.has_key('interactive') == False and request.args.has_key('blog') == True:
            setScripts()
            g.root.make_links_absolute(d['foruri'], resolve_base_href=True)
            g.root.body.set('onload', 'a11ypi.loadOverlay();')
        response = make_response()
        response.data = lxml.html.tostring(g.root)
        return response
    else:
        session['params'] = request.args.to_dict()
        return redirect(url_for('verify'))


@app.route("/verify", methods=["GET", "POST"])
def verify():
    if request.method == "GET":
        return render_template('verify.html')
    else:
        captcha_string = request.form.get('g-recaptcha-response')
        gVerify = requests.get(conf.RECAPTCHA_URL + "?secret=" + conf.RECAPTCHA_SECRET + "&response=" + captcha_string)
        if gVerify.json()['success'] is True:
            print (session.get('params'))
            response = make_response(redirect(url_for('start_page', **session.get('params'))))
            response.set_cookie('verified', 'True', 1800)
            return response
        else:
            return redirect(url_for('verify'))

def setScripts():
    print("*************inside setScripts*****************")
    script_test = g.root.makeelement('script')
    script_test.set("src", conf.APPURL[0] + "static/annolet.js")
    script_test.set("type", "text/javascript")
    g.root.body.append(script_test)
    print (script_test)

    
@app.route("/language-translive", methods=['POST'])
def languagetranslive():
    try:
        sentence = request.json['sentence']
    except:
        return "sentence parameter not passed"
    try :
        fromlang = request.json['from-language']
    except:
        return "en"
    try :
        tolang = request.json['to-language']
    except:
        return "to language not passed"

    res = req.get('https://translate.yandex.net/api/v1.5/tr/translate?key='+translatekey+'&text='+sentence+'&lang='+fromlang+'-'+tolang+'&format=plain&options=0');
    soup = bs4.BeautifulSoup(res.text)
    ret = soup.text
    return ret;

@app.route('/test', methods=['GET'])
def testApi():
	return "Its working...!"

if __name__ == '__main__':
    app.run(host=conf.HOST[0], port=conf.PORT[0], debug = True)
