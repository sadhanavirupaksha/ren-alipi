annoletContainer()
oprtnsTooltip()
toolTip()
addEventstoElems()

var operation = "";
var node_selected = "";
var sss = { "seg1" : {}, "seg2": [], "seg3": {} };

// Function to create container for annolet
function annoletContainer(){
    var body = document.getElementsByTagName('body')[0];
    var head = document.getElementsByTagName('head')[0];
    // Inject jquery 
    var scripttag = document.createElement('script');
    scripttag.type = 'text/javascript';
    scripttag.src = '//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'; 
    head.appendChild(scripttag);
    // Inject Bootsrap Js
    var bstrapJs_script = document.createElement('script');
    bstrapJs_script.type = 'text/javascript';
    bstrapJs_script.src = '//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js'; 
    head.appendChild(bstrapJs_script);
    // Inject Bootstrap css
    var bstrapCss_tag = document.createElement('link');
    bstrapCss_tag.href = '//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css'
    bstrapCss_tag.rel = 'stylesheet';
    head.appendChild(bstrapCss_tag);
    // Inject annolet css
    var annoletCss_tag = document.createElement('link');
    annoletCss_tag.href = 'https://gl.githack.com/sadhanavirupaksha/ren-alipi/raw/master/rnrserver/static/css/annolet.css';
    annoletCss_tag.rel = 'stylesheet';
    head.appendChild(annoletCss_tag);
    //appending a div(annolet container) to body element of a webpage.
    var container = document.createElement('div');
    container.id = 'sticky-container';
    container.className = 'anno';
    body.appendChild(container);
    //injecting html code
    container.innerHTML =  '<ul id="annoletElems" class="anno">'+
                                '<li id="struct-elem" class="anno"><img id="struct" class="anno" src="https://gl.githack.com/sadhanavirupaksha/ren-alipi/raw/master/rnrserver/static/images/struct-raw.png" alt="structure"></li>'+
                                '<li id="style-elem" class="anno" data-toggle="tooltip" data-placement="right" title="Style"><img id="style" class="anno" src="https://gl.githack.com/sadhanavirupaksha/ren-alipi/raw/master/rnrserver/static/images/style-raw.png" alt="style"></li>'+
                                '<li id="semantic-elem" class="anno" data-toggle="tooltip" data-placement="right" title="Semantic" onClick="langTrans();"><img id="semantic" class="anno" src="https://gl.githack.com/sadhanavirupaksha/ren-alipi/raw/master/rnrserver/static/images/semantic-raw.png" alt="semantic"></li>'+
                                '<li id="save-elem" class="anno" data-toggle="tooltip" data-placement="right" title="Save!"><span class="glyphicon glyphicon-save-file anno"></span></li>'+
                            '</ul>';
}

function oprtnsTooltip() {
    var body = document.getElementsByTagName('body')[0];
    var container = document.createElement('div');
    container.id = 'oprtns-tooltip';
    container.className = 'anno';
    body.appendChild(container);
    container.innerHTML =   '<h3 id="oprtn-listHeader" class="anno">List of operations</h3>'+
                            '<ul id="oprtns-list" class="anno">'+
                                '<li id="add-oprtn" class="anno">Add</li>'+
                                '<li id="del-oprtn" class="anno">Delete</li>'+
                                '<li id="repl-oprtn" class="anno">Replace</li>'+
                                '<li id="mod-oprtn" class="anno">Modify</li>'
                            '</ul>';
}

function toolTip() {
    var body = document.getElementsByTagName('body')[0];
    var container = document.createElement("div");
    container.id = "tooltip";
    container.className = "anno";
    body.appendChild(container);
    container.innerHTML =   '<form class="anno" >'+
                                '<p class="anno" id="selOprtn" >Operation Selected:'+operation+'</p>'+
                                '<button type="button" id="submitBtn" class="btn btn-primary" >Submit</button>&nbsp;'+
                                '<button type="button" id="viewBtn" class="btn btn-success" >View</button>'+
                            '</form>'; 
}

function addEventstoElems() {
    $("#struct-elem").click(function(){
        $("#oprtns-tooltip").toggle();
    });
    // adds mouse event to the elements
    var all = document.body.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
        if( !(all[i].className == "anno"||all[i].id == "submitBtn") ) {
            all[i].addEventListener("mouseup", function(e) {
               getContent(e)
            });
        }
    }
    $("#del-oprtn").click(function(){
        operation = "del";
        $("#selNode").remove();
        $('<p class="anno" id="selNode" >Node Selected:&nbsp;&nbsp;'+node_selected+'</p>').insertAfter('#selOprtn');
    });
    $("#add-oprtn").click(function(){
        operation = "add";
    });
    $("#submitBtn").click(function(){
        sssData()
    });
    $("#viewBtn").click(function(){
        viewSss()
    });    
}

var current;
function getContent(e) {
    current = getSelectedText();
    if(current != "") {
        var x = e.pageX;
        var y = e.pageY;
        placeTooltip(x, y);
        $("#tooltip").show();
    }
    else if(current == "") {
        $("#tooltip").hide();
    } 
}

function getSelectedText(){ 
    if(window.getSelection){
        node_selected = window.getSelection().focusNode.tagName;
        console.log(node_selected);
        return window.getSelection().toString(); 
    } 
    else if(document.getSelection){
        return document.getSelection(); 
    } 
    else if(document.selection){
        return document.selection.createRange().text; 
    }
    return ''; 
}

function placeTooltip(x_pos, y_pos) {
  $("#tooltip").css({
    top: y_pos + 'px',
    left: x_pos + 'px',
    position: 'absolute'
  });
}

function sssData() {
    var page_url = window.location.href;
    sss.seg1["renurl"] = page_url;
    if(operation == "del"){
        sss.seg2.push( {"obj" : {"selector"  : ""}, "actn": {"operation" : operation} } );
        console.log(operation);
    }
    else if(operation == "add"){
        sss.seg2.push( {"obj" : {"selector"  : node_selected}, "actn": {"operation" : operation}, "data": {} } );
        console.log(operation);
    }
}

function viewSss() {
    var rensss = JSON.stringify(sss);
    alert(rensss);
}

function getSelectedText(){ 
    if(window.getSelection){
        return window.getSelection().toString(); 
    } 
    else if(document.getSelection){
        return document.getSelection(); 
    } 
    else if(document.selection){
        return document.selection.createRange().text; 
    }
    return ''; 
}


function langTrans(){
    var from_lang = "en";
    var to_lang = "hi";
    var current = getSelectedText();
    if(current != "") { 
        var url = "http://127.0.0.1:5000/language-translive";
        var xhr = new XMLHttpRequest();
        xhr.open("POST",url,true);
        xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
        xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        xhr.send(JSON.stringify( {"sentence":current, "from-language":from_lang, "to-language":to_lang} ));
        xhr.onreadystatechange = function() {
            if (this.readyState==4 && this.status==200) {
                var res = this.responseText;
                var span_elem = document.createElement('span');
                span_elem.textContent = res;
                span_elem.style.backgroundColor = "yellow";
                span_elem.id = 'highlighted';
                var range = window.getSelection().getRangeAt(0);
                range.deleteContents();
                range.insertNode(span_elem);
            }
        }
    }
    else if(current == "") {
        alert("Select the text and click on 'Translate Text' button");
    }
}
